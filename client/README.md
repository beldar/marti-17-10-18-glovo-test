# Glovo Lite

## Features

* Code linting
* Some tests with Jest and Enzyme
* Mainly functional components
* CSS Modules
* Minimal tooling
* Redux state management
* Redux thunk middleware for API fetches

## Approach

When starting a new application from scratch I normally approach it by using the bare minimum number of things that I need to accomplish the task that I'm given.

Because applications tend to grow very quickly in features and files, by not having a lot of bloat at the begining it allows for a controlled growth and quick refactor.

With the same idea I've engineered the tooling around it. Instead of starting with a boilerplate that could do a lot of fancy things with lots of plugins, I've opted for an extrmely simple webpack configuration (arguably the simplest you can get), that would simply compile my frontend code to a bundle file.

On the UI/UX I took the most minimalist approach with subtle animations to enhance the interactivity. I've tried to mimic Glovo's design, colour palette and fonts.

## Testing

In the interest of time, I've opted to test only a few bits of code on the frontend. Showcasing things like snapshots, shallow render tests, and testing of asynchronous thunks.

## Install

```
npm install
```

## Run

```
npm start
```

## Develop

```
npm run dev
```

## Test

```
npm test
```

## Lint

```
npm run lint
```

## Notes

- My initial plan was to fetch the stores of the category once the user clicks on it. But since there is a constrain to show if the category is "closed" I was forced to fetch all the stores for all the categories up front on page load.

- Assuming the `day` property on a store follows the same convention as `date.getDay()`, where Sunday has index `0`.
