import React from 'react';
import PropTypes from 'prop-types';
import { category, categoryIcon, categoryLabel } from './Category.css';

const Category = ({ cat, showCat }) => (
  <li
    className={category}
    key={cat.name}
    onClick={() => showCat(cat)}
  >
    <img className={categoryIcon} src={cat.isOpen ? cat.openIcon : cat.sleepIcon} alt="" />
    <span className={categoryLabel}>{cat.label}</span>
  </li>
);

Category.propTypes = {
  cat: PropTypes.object.isRequired,
  showCat: PropTypes.func.isRequired
};

export default Category;
