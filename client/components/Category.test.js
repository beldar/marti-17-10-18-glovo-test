import React from 'react';
import { shallow } from 'enzyme';
import Category from './Category';

describe('Category', () => {
  it('should show a category name and open icon if its open', () => {
    const props = {
      cat: {
        isOpen: true,
        openIcon: Symbol('openIcon'),
        name: 'cat',
        label: 'Cat'
      },
      showCat: _ => _
    };
    const comp = shallow(<Category {...props} />);
    expect(comp.find('img').props().src).toEqual(props.cat.openIcon);
    expect(comp.find('span').text()).toEqual(props.cat.label);
  });

  it('should show the sleep icon if the category is not open', () => {
    const props = {
      cat: {
        isOpen: false,
        openIcon: Symbol('openIcon'),
        sleepIcon: Symbol('sleepIcon'),
        name: 'cat',
        label: 'Cat'
      },
      showCat: _ => _
    };
    const comp = shallow(<Category {...props} />);
    expect(comp.find('img').props().src).toEqual(props.cat.sleepIcon);
    expect(comp.find('span').text()).toEqual(props.cat.label);
  });

  it('should call showCat when its clicked', () => {
    const props = {
      cat: {
        isOpen: true,
        openIcon: Symbol('openIcon'),
        name: 'cat',
        label: 'Cat'
      },
      showCat: jest.fn()
    };
    const comp = shallow(<Category {...props} />);
    comp.simulate('click');
    expect(props.showCat).toHaveBeenCalledWith(props.cat);
  });
});
