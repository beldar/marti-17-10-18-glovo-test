import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loader from './Loader';
import Category from './Category';
import StoreList from './StoreList';
import { categoryList } from './CategoryList.css';

class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCat: false
    };
  }

  componentDidMount() {
    const { categories, getCategories } = this.props;
    if (!categories.length) {
      getCategories();
    }
  }

  showCat(cat) {
    const { showCat } = this.state;
    if (showCat) return false;

    this.setState({
      showCat: cat
    });
  }

  hideCat() {
    this.setState({
      showCat: false
    });
  }

  render() {
    const { categories } = this.props;
    const { showCat } = this.state;

    return (
      <div className={categoryList}>
        {
          showCat
            ? <StoreList cat={showCat} hideCat={this.hideCat.bind(this)} />
            : null
        }
        { categories.length ? (
          <ul>
            {categories.map(c => (
              <Category
                key={c.name}
                cat={c}
                showCat={this.showCat.bind(this)}
              />
            ))}
          </ul>
        ) : (
          <Loader width="100px" />
        )}
      </div>
    );
  }
}

CategoryList.propTypes = {
  categories: PropTypes.array.isRequired,
  getCategories: PropTypes.func.isRequired
};

export default CategoryList;
