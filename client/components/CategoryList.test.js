import React from 'react';
import { shallow } from 'enzyme';
import CategoryList from './CategoryList';

describe('CategoryList', () => {
  it('should render a loader if there are no categories, and call getCategories', () => {
    const getCategories = jest.fn();
    const comp = shallow(<CategoryList categories={[]} getCategories={getCategories} />);
    expect(comp.find('Loader').length).toEqual(1);
    expect(getCategories).toHaveBeenCalled();
    expect(comp).toMatchSnapshot();
  });

  it('should render the categories', () => {
    const getCategories = jest.fn();
    const categories = [{ name: '1' }, { name: '2' }, { name: '3' }];
    const comp = shallow(<CategoryList categories={categories} getCategories={getCategories} />);
    expect(comp.find('Category').length).toEqual(categories.length);
    expect(getCategories).not.toHaveBeenCalled();
    expect(comp).toMatchSnapshot();
  });

  it('should show the StoreList if a category is shown', () => {
    const getCategories = jest.fn();
    const categories = [{ name: '1' }, { name: '2' }, { name: '3' }];
    const comp = shallow(<CategoryList categories={categories} getCategories={getCategories} />);
    comp.instance().showCat(categories[0]);
    expect(comp.find('StoreList').length).toEqual(1);
    expect(comp).toMatchSnapshot();
  });
});
