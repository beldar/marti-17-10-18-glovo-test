import React from 'react';
import PropTypes from 'prop-types';
import svg from '../../public/img/loader.svg';

const Loader = ({ width }) => (
  <div className="loader">
    <img width={width} alt="loading content" src={svg} />
  </div>
);

Loader.propTypes = {
  width: PropTypes.string.isRequired
};

export default Loader;
