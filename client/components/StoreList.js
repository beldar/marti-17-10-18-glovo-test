import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  storeList, catIcon, close, storeName, openInfo, disappearing, tags
} from './StoreList.css';

class StoreList extends Component {
  constructor(props) {
    super(props);
    const { cat } = this.props;

    this.state = {
      stores: cat.stores,
      disappear: false
    };
  }

  hide() {
    const { hideCat } = this.props;

    this.setState({
      disappear: true
    }, () => {
      setTimeout(() => {
        hideCat();
      }, 300);
    });
  }

  filterStores(e) {
    const tag = e.target.value;
    const { cat } = this.props;
    let filteredStores = cat.stores;

    if (tag !== 'all') {
      filteredStores = cat.stores.filter(s => s.tags.includes(tag));
    }
    this.setState({
      stores: filteredStores
    });
  }

  render() {
    const { stores, disappear } = this.state;
    const { cat } = this.props;

    if (!stores) {
      return null;
    }

    const classes = [storeList];

    if (disappear) {
      classes.push(disappearing);
    }

    return (
      <div className={classes.join(' ')}>
        <button
          type="button"
          className={close}
          aria-label="close"
          onClick={() => this.hide()}
        >
          &times;
        </button>
        <h2>
          <img className={catIcon} src={cat.openIcon} alt="" />
          {cat.label}
        </h2>
        <div className={tags}>
          <label htmlFor="tags">Filter by: </label>
          <select onChange={this.filterStores.bind(this)} id="tags">
            <option value="all">all</option>
            {
              cat.tags.map(tag => <option key={tag} value={tag}>{tag}</option>)
            }
          </select>
        </div>
        <ul>
          {
            stores.map(store => (
              <li key={store.name}>
                <span className={storeName}>{store.name}</span>
                <span className={openInfo}>
                  {
                    store.isOpen
                      ? 'Open right now'
                      : `Next opening time: ${store.nextOpen}`
                  }
                </span>
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}

StoreList.propTypes = {
  cat: PropTypes.object.isRequired,
  hideCat: PropTypes.func.isRequired
};

export default StoreList;
