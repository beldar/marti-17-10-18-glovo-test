import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CategoryList from '../components/CategoryList';
import * as actions from '../state/actions';
// eslint-disable-next-line no-unused-vars
import css from './App.css';

const App = props => (
  <main id="app">
    <div id="logo" />
    <CategoryList {...props} />
  </main>
);

const mapStateToProps = state => ({
  categories: state.categories.categories,
  isBusy: state.categories.isBusy,
  apiError: state.categories.apiError
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
