const WEEK = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const getOpenData = (schedule) => {
  schedule.sort((a, b) => {
    if (a.day > b.day) {
      return 1;
    }
    if (a.day < b.day) {
      return -1;
    }
    return 0;
  });
  const now = new Date();
  const day = now.getDay();
  const dateMinutes = now.getMinutes();
  const minutes = dateMinutes < 10 ? `0${dateMinutes}` : dateMinutes;
  const hour = parseInt(`${now.getHours()}${minutes}`, 10);
  const today = schedule.find(h => h.day === day);
  let isOpen;
  let nextOpen = '';

  if (!today || !today.open || !today.close) {
    isOpen = false;
    let nextDay = schedule.find(h => h.day > day);
    if (!nextDay) {
      nextDay = schedule[0];
    }
    if (!nextDay) {
      nextOpen = 'Unkown';
    } else {
      nextOpen = `${WEEK[nextDay.day]} at ${nextDay.open}`;
    }
  } else {
    const open = parseInt(today.open.replace(':', ''), 10);
    const close = parseInt(today.close.replace(':', ''), 10);
    isOpen = hour > open && hour < close;

    if (!isOpen) {
      if (hour < open) {
        nextOpen = `${WEEK[day]} at ${today.open}`;
      }

      if (hour > close) {
        const dayIndex = schedule.indexOf(today);
        let nextDay;
        if (dayIndex + 1 === schedule.length) {
          nextDay = schedule[0];
        } else {
          nextDay = schedule[dayIndex + 1];
        }
        nextOpen = `${WEEK[nextDay.day]} at ${nextDay.open}`;
      }
    }
  }

  return {
    isOpen,
    nextOpen
  };
};

export default getOpenData;
