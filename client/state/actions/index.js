import getOpenData from '../../helpers/getOpenData';

export const SET_CATEGORIES = 'SET_CATEGORIES';
export const BEGIN_FETCH = 'BEGIN_FETCH';
export const FETCH_FAILURE = 'FETCH_FAILURE';

export const setCategories = categories => ({
  type: SET_CATEGORIES,
  payload: categories
});

export const beginFetch = () => ({
  type: BEGIN_FETCH
});

export const fetchFailure = error => ({
  type: FETCH_FAILURE,
  payload: error
});


export const getStores = ({ categories }) => Promise.all(categories.map(cat => (
  fetch(`/stores?category=${cat.name}`)
    .then(r => r.json())
    .then(({ stores }) => {
      cat.tags = cat.tags || [];
      cat.isOpen = false;
      cat.stores = stores.map((store) => {
        const openData = getOpenData(store.schedule);
        store.isOpen = openData.isOpen;
        store.nextOpen = openData.nextOpen;
        if (store.isOpen) {
          cat.isOpen = true;
        }
        store.tags.forEach((t) => {
          if (!cat.tags.includes(t)) {
            cat.tags.push(t);
          }
        });
        return store;
      });
      cat.stores.sort((a, b) => {
        if (!a.isOpen && b.isOpen) {
          return 1;
        }
        if (a.isOpen && !b.isOpen) {
          return -1;
        }
        return 0;
      });
      return cat;
    })
)));

export const getCategories = () => (dispatch) => {
  dispatch(beginFetch());
  return fetch('/categories')
    .then((r) => {
      if (r.status >= 400) {
        return r.json().then((error) => { throw new Error(error.error); });
      }
      return r;
    })
    .then(r => r.json())
    .then(getStores)
    .then((categories) => {
      dispatch(setCategories(categories));
    })
    .catch((error) => {
      console.error(error);
      dispatch(fetchFailure(error.message));
    });
};
