import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {
  getCategories,
  SET_CATEGORIES,
  BEGIN_FETCH,
  FETCH_FAILURE
} from './index';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
let cons;
describe('async actions', () => {
  beforeEach(() => {
    cons = console.error;
    console.error = jest.fn();
  });

  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
    console.error = cons;
  });

  describe('getCategories', () => {
    it('should dispatch the right actions when successfuly fetching the categories', () => {
      fetchMock
        .getOnce('/categories', { body: { categories: [] }, headers: { 'content-type': 'application/json' } });

      const expectedActions = [
        { type: BEGIN_FETCH },
        { type: SET_CATEGORIES, payload: [] }
      ];

      const store = mockStore({ categories: [] });

      return store.dispatch(getCategories()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should dispatch the right actions when uploading a file fails', () => {
      const errorMessage = 'Something went wrong';
      fetchMock
        .getOnce('/categories', { status: 500, body: { error: errorMessage }, headers: { 'content-type': 'application/json' } });

      const expectedActions = [
        { type: BEGIN_FETCH },
        { type: FETCH_FAILURE, payload: errorMessage }
      ];

      const store = mockStore({ categories: [] });

      return store.dispatch(getCategories()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should fetch the stores for each category', () => {
      const cat = {
        id: 3,
        label: 'Snacks',
        name: 'snacks',
        openIcon: 'http://localhost:3000/img/snacks-open.png',
        sleepIcon: 'http://localhost:3000/img/snacks-sleep.png'
      };

      fetchMock
        .getOnce('/categories', { body: { categories: [cat] }, headers: { 'content-type': 'application/json' } })
        .getOnce('/stores?category=snacks', { body: { stores: [] }, headers: { 'content-type': 'application/json' } });

      const expectedActions = [
        { type: BEGIN_FETCH },
        {
          type: SET_CATEGORIES,
          payload: [{
            ...cat,
            tags: [],
            stores: [],
            isOpen: false
          }]
        }
      ];

      const store = mockStore({ categories: [] });

      return store.dispatch(getCategories()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
