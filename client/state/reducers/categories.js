import {
  SET_CATEGORIES,
  BEGIN_FETCH,
  FETCH_FAILURE
} from '../actions';

const defaultState = {
  categories: [],
  isBusy: false,
  apiError: false
};

const categories = (state = defaultState, action) => {
  switch (action.type) {
    case SET_CATEGORIES:
      return Object.assign({}, state, {
        categories: action.payload
      });
    case BEGIN_FETCH:
      return Object.assign({}, state, {
        isBusy: true,
        apiError: false
      });
    case FETCH_FAILURE:
      return Object.assign({}, state, {
        isBusy: false,
        apiError: action.payload
      });
    default:
      return state;
  }
};

export default categories;
